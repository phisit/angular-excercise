import { AngularExcercisePage } from './app.po';

describe('angular-excercise App', () => {
  let page: AngularExcercisePage;

  beforeEach(() => {
    page = new AngularExcercisePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
