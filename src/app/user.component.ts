import { Component, Input, Output, EventEmitter } from '@angular/core';
/*
  Lesson 2.18 Listen to User Event one-way binding and two way binging
*/
@Component({
  selector: 'app-user',
  template: `
    <!-- Old Oneway binding -->
    <input type="text" (input)="onUserInput($event)" [value]="name">

    <!-- Lesson 2.22
    <input type="text" [(ngModel)]="name"> -->
    <p>Hello {{ name }} !!!</p>
    <p>User Component</p>
  `
})
export class UserComponent {
  /*
    Lesson 2.22 Change Variable from main app component
  */
  @Input() name;
  @Output() nameChange = new EventEmitter<string>();

  onUserInput(event) {
    this.nameChange.emit(event.target.value);
  }

  /* disable on event
  onUserInput(event) {
    this.name = event.target.value;
  }
  */
}
