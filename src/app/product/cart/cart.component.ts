import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @Input() items = [];
  @Output() onItemSubmit = new EventEmitter<string>();

  itemName = '';
  a = true;

  constructor() { console.log('constructor'); }

  ngOnInit() {
    console.log('ngOnInit');
  }

  onAddItem(event) {
    this.onItemSubmit.emit(this.itemName);
    this.itemName = '';
  }
}
