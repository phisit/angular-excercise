import { Component } from '@angular/core';
/*
  Assignment Dashboard Component
*/
@Component({
  selector: 'app-dashboard',
  template: `
    <p>Dashboard : {{ loadState }}</p>
    <input type="button" (click)="onClick()" [(ngModel)]="loadState">
  `
})
export class DashboardComponent {
  loadState = 'loading';

  onClick() {
    this.loadState = 'finished';
  }
}
