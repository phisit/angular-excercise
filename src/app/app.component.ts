import { Component } from '@angular/core';
import { random } from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Excercise';
  firebase: any;

  rootName = 'Phisit.k';
  rootItem = ['Product A', 'Product B'];
  number = 0;

  constructor() {
    this.firebase = window['firebase'] || {};
    console.log('firebase', this.firebase);
  }

  onNameChange(newName) {
    this.rootName = newName;
  }
  onItemSubmitEvent(newItem) {
    console.log('onItemSubmitEvent', newItem);
    this.rootItem.push(newItem);
  }
  increaseNumber() {
    this.number = this.number + random(1, 10);
  }
}
