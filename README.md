# AngularExcercise

### Learning Video
- 1.1
- 1.2
- 1.3
- 1.4
- 1.5
- 1.6
- 1.7 - https://www.udemy.com/angular-essentials-angular-2-angular-4-with-typescript/learn/v4/t/lecture/7457010?start=0
- 1.8 - https://www.udemy.com/angular-essentials-angular-2-angular-4-with-typescript/learn/v4/t/lecture/7457018?start=0

- 2.12 - https://www.udemy.com/angular-essentials-angular-2-angular-4-with-typescript/learn/v4/t/lecture/7457096?start=0
- 2.25
- 3.33 - Wrap UP https://www.udemy.com/angular-essentials-angular-2-angular-4-with-typescript/learn/v4/t/lecture/7457452?start=0
- 4.41 - Wrap UP Structural directives vs attribute directives

# Useful Resources & Links

### Section 2, Lecture 26
 - Understanding the Angular Architecture: https://angular.io/guide/architecture
 - Displaying Data: https://angular.io/guide/displaying-data
 - A closer look at the template syntax: https://angular.io/guide/template-syntax
 - Understanding the Component Lifecycle: https://angular.io/guide/lifecycle-hooks
 - Understanding Component Interaction: https://angular.io/guide/component-interaction
 - How to style Components: https://angular.io/guide/component-styles
#### Resources for this lecture
 components-addition.zip
 components-pass-data.zip
 components-basics.zip

### Section 4, Lecture 42
 - More about structural directives (like *ngFor): https://angular.io/guide/structural-directives
 - More about attribute directives (like ngClass): https://angular.io/guide/attribute-directives
#### Resources for this lecture
 directives.zip
